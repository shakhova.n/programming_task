str = input("Введите выражение: ")

#Убрать пробелы

str = ''.join(str.split())

character_array = list(str)

count = len(character_array)

# Математические функции

#Деление и умножение

def divide_multiply (character_array, i, count):
    if character_array[i] == '/':
        if float (character_array[i+1]) == 0:
            print ("Найдено деление на 0. Ответ +- бесконечность")
            quit()
        character_array [i] = float (character_array[i-1]) / float (character_array[i+1])
        del character_array [i-1], character_array [i]
        count = len(character_array)
        i -= 1
    elif character_array[i] == '*':
        character_array [i] = float (character_array[i-1]) * float (character_array[i+1])
        del character_array [i-1], character_array [i]
        count = len(character_array)
        i -= 1
    return (character_array, i, count)

#Сложение и вычитание

def plus_minus (character_array, i, count):
    if character_array[i] == '+':
        character_array [i] = float (character_array[i-1]) + float (character_array[i+1])
        del character_array [i-1], character_array [i]
        count = len(character_array)
        i -= 1
    elif character_array[i] == '-':
        character_array [i] = float (character_array[i-1]) - float (character_array[i+1])
        del character_array [i-1], character_array [i]
        count = len(character_array)
        i -= 1
    return (character_array, i, count)

#Проверка чисел на многозначность

m=0
while m < len (character_array)-1:
    if character_array [m] != '-' and character_array [m] != '+' and character_array [m] != '/' and character_array [m] != '*' and character_array [m] != '(' and character_array [m] != ')':
        if character_array [m+1] == '0' or character_array [m+1] == '1' or character_array [m+1] == '2' or character_array [m+1] == '3' or character_array [m+1] == '4' or character_array [m+1] == '5' or character_array [m+1] == '6' or character_array [m+1] == '7' or character_array [m+1] == '8' or character_array [m+1] == '9':
            character_array [m] = float (character_array [m]) * 10 + float (character_array [m+1])
            del character_array [m+1]
            m -= 1
    m += 1

#Проверка отрицательности числа

def proverka_otricania (character_array, i, count):
    i = 0
    while i < count:
        if character_array[i] == '-':
            if (i == 0 and character_array[i+1]!='(') or character_array[i-1] == '/' or character_array[i-1] == '*':
                character_array [i+1] = 0 - float (character_array [i+1])
                del character_array [i]
            elif character_array[i-1] == '(' and character_array[i+2] == ')':
                character_array [i+1] = 0 - float (character_array [i+1])
                del character_array [i-1], character_array [i-1], character_array [i]
            elif character_array[i-1] == '(' and (character_array[i+2] == '-' or character_array[i+2] == '+' or character_array[i+2] == '/' or character_array[i+2] == '*'):
                character_array [i+1] = 0 - float (character_array [i+1])
                del character_array [i]
        count = len(character_array)
        i += 1
    return (character_array, i, count)

i=0
character_array, i, count = proverka_otricania (character_array, i, count)

#Проверка содержимого скобок

def skobki (character_array, count):

    while "(" in character_array:
            start = character_array.index("(")
            end = start + 1
            l = 1
            while l > 0:
                if character_array[end] == "(":
                    l += 1
                elif character_array[end] == ")":
                    l -= 1
                end += 1
            sub_character = character_array[start+1:end-1]
            k = 0
            while k < len (sub_character):
                if sub_character[k] == '(':
                    sub_character, _ = skobki (sub_character, len (sub_character))
                    count = len (character_array)
                elif sub_character [k] == '/' or sub_character [k] == '*':
                    sub_character, k, _ = divide_multiply(sub_character, k, len(sub_character)-1)
                k += 1
            k=0
            while k < len (sub_character):
                if sub_character [k] == '+' or sub_character [k] == '-':
                    i=0
                    sub_character, k, _ = plus_minus(sub_character, k, len(sub_character)-1)
                k += 1
            character_array[start:end] = sub_character[:]
            count = len (character_array)
    return (character_array, count)

character_array, count = skobki (character_array, count)

character_array, i, count = proverka_otricania (character_array, i, count)

# Проверки выражений после раскрытия скобок

i = 0
while i < count:
    character_array, i, count = divide_multiply (character_array, i, count)
    i += 1

i = 0
while i < count:
    character_array, i, count = plus_minus (character_array, i, count)
    i += 1

print("Result", character_array)